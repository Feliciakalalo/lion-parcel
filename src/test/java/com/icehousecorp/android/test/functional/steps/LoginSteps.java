package com.icehousecorp.android.test.functional.steps;

import com.icehousecorp.android.test.functional.Behavior.Login;
import com.icehousecorp.android.test.functional.Behavior.LoginBH;
import com.icehousecorp.android.test.functional.Behavior.PickupBH;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class LoginSteps {
    @Steps
    LoginBH lgHome;

    @Steps
    Login lg;


    @Steps
    PickupBH pickPage;

    @Given("^the user on the home screen$")
    public void theUserOnTheHomeScreen() {
        lgHome.openHomeMenu();

    }

    @When("^the user tap on side menu$")
    public void theUserTapOnSideMenu() {
        lgHome.openHambugerMenu();

    }

    @Then("^the user tap the button masuk on side menu$")
    public void theUserTapTheButtonMasukOnSideMenu(){
        lgHome.openLogin();

    }



    @Given("^the user on the home screen(\\d+)$")
    public void theUserOnTheHomeScreen(int arg0) {
        lgHome.openHomeMenu();
    }

    @When("^the user tap on side menu(\\d+)$")
    public void theUserTapOnSideMenu(int arg0) {
        lgHome.openHambugerMenu();
    }


    @Then("^the user tap the button daftar on side menu$")
    public void theUserTapTheButtonDaftarOnSideMenu()  {
        lgHome.openDaftar();

    }

    @Given("^the user see on the home screen and have account$")
    public void theUserSeeOnTheHomeScreenAndHaveAccount() {
        lgHome.openHomeMenu();
        lgHome.openHambugerMenu();
        lgHome.openLogin();


    }

    @When("^the user go to login and input all the textfield$")
    public void theUserGoToLoginAndInputAllTheTextfield()  {
        lg.inputPhoneNumber();
        lg.inputPassword();
        lg.clickToogle();

    }

    @Then("^the user tap the login button to next step$")
    public void theUserTapTheLoginButtonToNextStep() {
        lg.clickMasuk();

    }


    @Given("^the user on the home screen and go to Pickup Request$")
    public void theUserOnTheHomeScreenAndGoToPickupRequest() {
        lgHome.openHomeMenu();
        lgHome.openPickUp();
        pickPage.clickSelanjutnya();


    }

    @When("^the map default current location and go to selanjutnya$")
    public void theMapDefaultCurrentLocationAndGoToSelanjutnya() {
        pickPage.clickRequestBtnMap();

    }


    @Then("^Input quantity order pickup$")
    public void inputQuantityOrderPickup() {
        pickPage.inputQuantityMap();
    }
}
