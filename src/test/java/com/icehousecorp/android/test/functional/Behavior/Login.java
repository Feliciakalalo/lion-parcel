package com.icehousecorp.android.test.functional.Behavior;

import com.icehousecorp.android.test.functional.pages.LoginPage;
import net.thucydides.core.annotations.Step;

public class Login {

    LoginPage lgPage;

    @Step
    public void inputPhoneNumber(){
        lgPage.input_Phone_lg();
    }

    @Step
    public void inputPassword(){
        lgPage.input_password_lg();
    }

    @Step
    public void clickToogle(){
        lgPage.click_toogle_lg();
    }

    @Step
    public void clickMasuk(){
        lgPage.click_masuk_btn_lg();
    }

}
