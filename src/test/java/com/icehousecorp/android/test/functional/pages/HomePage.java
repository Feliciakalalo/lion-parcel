package com.icehousecorp.android.test.functional.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;



public class HomePage extends PageObject {

    @FindBy(xpath = "//android.widget.ImageButton[@content-desc=\"Navigasi naik\"]")
    private WebElementFacade hambuger_btn;


    @FindBy(id = "com.lionparcel.services.consumer.stage:id/btnLogin")
    private WebElementFacade masuk_btn;

    @FindBy(id = "com.lionparcel.services.consumer.stage:id/btnRegister")
    private WebElementFacade daftar_btn;


    @FindBy(id = "com.lionparcel.services.consumer.stage:id/pickUpMenu")
    private WebElementFacade pickUp_btn;

    @FindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.view.ViewGroup/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup/android.widget.FrameLayout[4]/android.widget.FrameLayout/android.view.ViewGroup\n")
    private WebElementFacade cekTarif_btn;

    public void click_hambuger_btn(){
        waitForAbsenceOf("//*[contains(@id, \"txtVersion\")]");
        this.waitFor(8000).milliseconds();
        hambuger_btn.click();

    }
    public void click_masuk_btn() {
        masuk_btn.click();
    }

    public void click_daftar_btn(){
        daftar_btn.click();
    }

    public void click_pickup_btn(){
        waitForAbsenceOf("//*[contains(@id, \"txtVersion\")]");
        this.waitFor(5000).milliseconds();
        pickUp_btn.click();

    }

    public void click_cekTarif_btn(){
        cekTarif_btn.click();
    }



}
