package com.icehousecorp.android.test.functional.Behavior;

import com.icehousecorp.android.test.functional.AndroidDriverSource;
import com.icehousecorp.android.test.functional.pages.HomePage;
import net.thucydides.core.annotations.Step;



public class LoginBH {

    HomePage home;

    @Step
    public void openHomeMenu() {
        home.setDriver(home.getDriver());
        System.out.print(AndroidDriverSource.ANDROID_DRIVER);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Step
    public void openHambugerMenu(){
        home.click_hambuger_btn();

    }

    @Step
    public void openLogin() {
        home.click_masuk_btn();

    }

    @Step
    public void openDaftar(){
        home.click_daftar_btn();
    }

    @Step
    public void openPickUp(){
        home.click_pickup_btn();
    }


}
