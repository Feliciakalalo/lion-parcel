package com.icehousecorp.android.test.functional.Behavior;

import com.icehousecorp.android.test.functional.pages.LoginPage;
import com.icehousecorp.android.test.functional.pages.PickUp;
import net.thucydides.core.annotations.Step;

public class PickupBH {

    PickUp pUpPage;

    @Step
    public void clickSelanjutnya(){
        pUpPage.setMap_Selanjutnya_btn();
    }

    @Step
    public void inputPhoneNumberMap(){
        pUpPage.input_Phone_txt();
    }

    @Step
    public void inputNameMap(){
        pUpPage.input_Name_txt();

    }
    @Step
    public void clickRequestBtnMap(){
        pUpPage.requestBtn();

    }

    @Step
    public void inputQuantityMap(){
        pUpPage.input_quantity_btn();
    }





}
