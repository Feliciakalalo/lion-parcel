package com.icehousecorp.android.test.functional.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class PickUp extends PageObject {


    @FindBy(id="com.lionparcel.services.consumer.stage:id/btnNext")
    private WebElementFacade map_Selanjutnya_btn;



    @FindBy(id="com.lionparcel.services.consumer.stage:id/btnRequest")
    private WebElementFacade request_btn;


    @FindBy(id="com.lionparcel.services.consumer.stage:id/btnAddShipmentDetail")
    private WebElementFacade tambah_pengiriman_btn;

    @FindBy(id="com.lionparcel.services.consumer.stage:id/edtQuantity")
    private WebElementFacade quantity_btn;


    @FindBy(id="com.lionparcel.services.consumer.stage:id/edtSenderName")
    private WebElementFacade sender_name_txt;


    @FindBy(id="com.lionparcel.services.consumer.stage:id/edtSenderPhone")
    private WebElementFacade sender_Phone_txt;



    public void setMap_Selanjutnya_btn () {
        this.waitFor(5000).milliseconds();
        map_Selanjutnya_btn.click();

    }

    public void requestBtn() {
        this.waitFor(5000).milliseconds();
        request_btn.click();

    }

    public void tambahPengiriman(){
        tambah_pengiriman_btn.click();
    }

    public void input_quantity_btn(){
        this.waitFor(5000).milliseconds();
        quantity_btn.type("15");

    }

    public void input_Name_txt(){
        this.waitFor(5000).milliseconds();
        sender_name_txt.type("Felicia");

    }

    public void input_Phone_txt(){
        this.waitFor(5000).milliseconds();
        sender_Phone_txt.type("0817621911");

    }


}

