package com.icehousecorp.android.test.functional.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class LoginPage extends PageObject {

    @FindBy(id="com.lionparcel.services.consumer.stage:id/edtPhoneNumber")
    private WebElementFacade lg_phoneNumber_txt;

    @FindBy(id = "com.lionparcel.services.consumer.stage:id/edtPassword")
    private WebElementFacade lg_password_txt;

    @FindBy(id = "com.lionparcel.services.consumer.stage:id/text_input_password_toggle")
    private WebElementFacade lg_toogle_eye;

    @FindBy(id = "com.lionparcel.services.consumer.stage:id/btnLogin")
    private WebElementFacade lg_masuk_btn;





    public void input_Phone_lg () {
        lg_phoneNumber_txt.type("0817621911");

    }

    public void input_password_lg(){
        lg_password_txt.type("123123123");
    }

    public void click_toogle_lg(){
        lg_toogle_eye.click();
    }

    public void click_masuk_btn_lg(){
        lg_masuk_btn.click();
    }




}